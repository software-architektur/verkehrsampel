import unittest
import blink

class TestLed(unittest.TestCase):
    def setUp(self):
        print("setup test...")
        self.ROT = 15
        self.GELB = 16
        self.GRUEN = 18
        blink.define_gpios(self.ROT, self.GELB, self.GRUEN, True)

    def tearDown(self):
        blink.clean(self.ROT, self.GELB, self.GRUEN)
        print("tearDown test...")

    def test_trun_on_led(self):
        print("\tTestCase trun_on_led")
        value = blink.turn_on_led(self.GELB)
        self.assertEqual(1, value)

    def test_trun_off_led(self):
        print("\tTestCase trun_off_led")
        value = blink.turn_off_led(self.GELB)
        self.assertEqual(0, value)

    def test_trun_on_all(self):
        print("\tTestCase trun_on_all")
        value = blink.turn_on_all(self.ROT, self.GELB, self.GRUEN, 2)
        self.assertEqual(1, value)

    def test_trun_off_alle(self):
        print("\tTestCase trun_off_all")
        value = blink.turn_off_all(self.ROT, self.GELB, self.GRUEN, 2)
        self.assertEqual(0, value)

    def test_blinkLed(self):
        print("\tTestCase blinkLed")
        value_on = blink.turn_on_led(self.GELB)
        self.assertEqual(1, value_on) 
        value_off = blink.turn_off_led(self.GELB)
        self.assertEqual(0, value_off)  

if __name__ == '__main__':
    print("\n-------------------------------------------------> Blinkled Unit Test <-------------------------------------------------")
    unittest.main()
