import pin
import time
import sys

def setConfig(status):
  pin.conf['test'] = status

def define_gpios(led1, led2, led3, status):
    setConfig(status)
    pin.setmode(pin.BOARD)
    pin.setup(led1, pin.OUT)
    pin.setup(led2, pin.OUT)
    pin.setup(led3, pin.OUT)


def turn_on_led(led):
  pin.output(led, pin.HIGH)
  if pin.conf['test'] == True:
    if pin.get_output(led) == 1:
      return 1
    else:
      return -1

def turn_off_led(led):
  pin.output(led, pin.LOW)
  if pin.conf['test'] == True:
    if pin.get_output(led) == 0:
      return 0
    else:
      return -1

def blinkLed(led, blink_interval):
    turn_on_led(led)
    time.sleep(blink_interval)
    turn_off_led(led)
    time.sleep(blink_interval)

def turn_off_all(led1, led2, led3, seconds):
  pin.output(led1, pin.LOW)
  pin.output(led2, pin.LOW)
  pin.output(led3, pin.LOW)
  time.sleep(seconds)
  if pin.conf['test'] == True:
    if (pin.get_output(led1) and pin.get_output(led2) and pin.get_output(led3)) == 0:
      return 0
    else:
      return -1

def turn_on_all(led1, led2, led3, seconds):
  pin.output(led1, pin.HIGH)
  pin.output(led2, pin.HIGH)
  pin.output(led3, pin.HIGH)
  time.sleep(seconds)
  if pin.conf['test'] == True:
    if (pin.get_output(led1) and pin.get_output(led2) and pin.get_output(led3)) == 1:
      return 1
    else:
      return -1

def clean(led1, led2, led3):
    pin.cleanup([led1, led2, led3])

if __name__ == '__main__':
    ROT = 15
    GELB = 16
    GRUEN = 18

    # Blink Interval 
    blink_interval = .5 #Zeit Interval in Sekunde

    print("Blinking ist eingeschaltet")

    # setConfig(True), wenn wir testen moechten

    define_gpios(ROT, GELB, GRUEN, False)

    turn_off_all(ROT, GELB, GRUEN, 2)
    clean(ROT, GELB, GRUEN)
    #   pin.cleanup([ROT, GELB, GRUEN])

    define_gpios(ROT, GELB, GRUEN, False)
    turn_on_all(ROT, GELB, GRUEN, 2)

    turn_off_all(ROT, GELB, GRUEN, 2)
    pin.cleanup([ROT, GELB, GRUEN])

    define_gpios(ROT, GELB, GRUEN, False)
    # ROT und GREUN muessen ausgeschaltet werden
    pin.output(ROT, pin.LOW)
    pin.output(GRUEN, pin.LOW)

    t_end = time.time() + (60 * 20)

  # Blinker Schleife
    while time.time() < t_end:
        try:
            blinkLed(GELB, blink_interval)
        except KeyboardInterrupt:
            print("Blinking ist manuell ausgeschaltet!")
            turn_off_all(ROT, GELB, GRUEN, 1)
            pin.cleanup([ROT, GELB, GRUEN])
            sys.exit(0)
      
# turn_off_all nach ende der Schleife
    turn_off_all(ROT, GELB, GRUEN, 1)
    pin.cleanup([ROT, GELB, GRUEN])
    sys.exit(0)
