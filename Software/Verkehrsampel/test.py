import unittest
import ampel

class TestAmpel(unittest.TestCase):
    def setUp(self):
        print("setup test...")
        self.ROT = 15
        self.GELB = 16
        self.GRUEN = 18
        ampel.define_gpios(self.ROT, self.GELB, self.GRUEN, True)

    def tearDown(self):
        ampel.clean(self.ROT, self.GELB, self.GRUEN)
        print("tearDown test...\n")

    def test_trun_on_led(self):
        print("\tTestCase trun_on_led")
        value = ampel.turn_on_led(self.GELB)
        self.assertEqual(1, value)

    def test_trun_off_led(self):
        print("\tTestCase trun_off_led")
        value = ampel.turn_off_led(self.GELB)
        self.assertEqual(0, value)
        
    def test_trun_on_all(self):
        print("\tTestCase trun_on_all")
        value = ampel.turn_on_all(self.ROT, self.GELB, self.GRUEN, 2)
        self.assertEqual(1, value)

    def test_trun_off_alle(self):
        print("\tTestCase trun_off_all")
        value = ampel.turn_off_all(self.ROT, self.GELB, self.GRUEN, 2)
        self.assertEqual(0, value)

    def test_ampel(self):
        print("\tTestCase ampel Anfang")
        # Phase eins
        value_on_rot = ampel.turn_on_led(self.ROT)
        self.assertEqual(1, value_on_rot)
        value_off_gelb = ampel.turn_off_led(self.GELB)
        self.assertEqual(0, value_off_gelb)
        value_off_gruen = ampel.turn_off_led(self.GRUEN)
        self.assertEqual(0, value_off_gruen)
        print("\t\tampel test Phase eins bestanden")
        
        # Phase zwei
        value_off_rot = ampel.turn_off_led(self.ROT)
        self.assertEqual(0, value_off_rot)
        value_on_gelb = ampel.turn_on_led(self.GELB)
        self.assertEqual(1, value_on_gelb)
        value_off_gruen = ampel.turn_off_led(self.GRUEN)
        self.assertEqual(0, value_off_gruen)
        print("\t\tampel test Phase zwei bestanden")

        # Phase drei
        value_off_rot = ampel.turn_off_led(self.ROT)
        self.assertEqual(0, value_off_rot)
        value_on_gelb = ampel.turn_on_led(self.GELB)
        self.assertEqual(1, value_on_gelb)
        value_on_gruen = ampel.turn_on_led(self.GRUEN)
        self.assertEqual(1, value_on_gruen)
        print("\t\tampel test Phase drei bestanden")

        print("\tTestCase ampel Ende")

if __name__ == '__main__':
    print("\n-------------------------------------------------> Verkehrsampel Unit Test <-------------------------------------------------")
    unittest.main()
  
