# Python Skript um alle GPIOs auszuschalten

# import RPi.GPIO as GPIO
import RPi.GPIO as GPIO
import sys
import time

#GPIO Modus (BOARD / BCM)
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
 
#Richtung der GPIO-Pins festlegen (IN / OUT)
ROT = 15
GELB = 16
GRUEN = 18

GPIO.setup(ROT, GPIO.OUT)
GPIO.setup(GELB, GPIO.OUT)
GPIO.setup(GRUEN, GPIO.OUT)

def turn_on_alle_leds(seconds):
  GPIO.output(ROT, True)
  GPIO.output(GELB, True)
  GPIO.output(GRUEN, True)
  time.sleep(seconds)

def turn_off():
  GPIO.output(ROT, False)
  GPIO.output(GELB, False)
  GPIO.output(GRUEN, False)
  GPIO.cleanup()

def define_gpio():
  #GPIO Modus (BOARD / BCM)
  GPIO.setmode(GPIO.BOARD)
  GPIO.setwarnings(False)
 
  #Richtung der GPIO-Pins festlegen (IN / OUT)
  ROT = 15
  GELB = 16
  GRUEN = 18

  GPIO.setup(ROT, GPIO.OUT)
  GPIO.setup(GELB, GPIO.OUT)
  GPIO.setup(GRUEN, GPIO.OUT)

#unendliche Schleife
try:
  turn_off()
except KeyboardInterrupt:
  turn_off()
  sys.exit(0)
  