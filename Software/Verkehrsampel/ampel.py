import pin
import time
import sys

def setConfig(status):
  pin.conf['test'] = status

def define_gpios(led1, led2, led3, status):
    setConfig(status)
    pin.setmode(pin.BOARD)
    pin.setup(led1, pin.OUT)
    pin.setup(led2, pin.OUT)
    pin.setup(led3, pin.OUT)

def turn_on_led(led):
  pin.output(led, pin.HIGH)
  if pin.conf['test'] == True:
    if pin.get_output(led) == 1:
      return 1
    else:
      return -1

def turn_off_led(led):
  pin.output(led, pin.LOW)
  if pin.conf['test'] == True:
    if pin.get_output(led) == 0:
      return 0
    else:
      return -1

def turn_off_all(led1, led2, led3, seconds):
    pin.output(led1, pin.LOW)
    pin.output(led2, pin.LOW)
    pin.output(led3, pin.LOW)
    time.sleep(seconds)
    if pin.conf['test'] == True:
        if (pin.get_output(led1) and pin.get_output(led2) and pin.get_output(led3)) == 0:
            return 0
        else:
            return -1

def turn_on_all(led1, led2, led3, seconds):
    pin.output(led1, pin.HIGH)
    pin.output(led2, pin.HIGH)
    pin.output(led3, pin.HIGH)
    time.sleep(seconds)
    if pin.conf['test'] == True:
        if (pin.get_output(led1) and pin.get_output(led2) and pin.get_output(led3)) == 1:
            return 1
        else:
            return -1

def clean(led1, led2, led3):
    pin.cleanup([led1, led2, led3])

def ampel():


  #Stage 1: Rot
    turn_on_led(ROT)
    turn_off_led(GELB)
    turn_off_led(GRUEN)
    time.sleep(4)
    #Stage 2: Gelb
    turn_off_led(ROT)
    turn_on_led(GELB)
    turn_off_led(GRUEN)
    time.sleep(2)
    #Stage 3: Gruen
    turn_off_led(ROT)
    turn_off_led(GELB)
    turn_on_led(GRUEN)
    time.sleep(4)
    #Stage 4: Gruen Gelb
    turn_off_led(ROT)
    turn_on_led(GELB)
    turn_on_led(GRUEN)
    time.sleep(2)

if __name__ == '__main__':
    ROT = 15
    GELB = 16
    GRUEN = 18

    # 4 Minuten
    t_end = time.time() + (60 * 20)
    
    define_gpios(ROT, GELB, GRUEN, False)
    turn_off_all(ROT, GELB, GRUEN, 2)
    clean(ROT, GELB, GRUEN)

    define_gpios(ROT, GELB, GRUEN, False)
    turn_on_all(ROT, GELB, GRUEN, 2)

    turn_off_all(ROT, GELB, GRUEN, 2)
    clean(ROT, GELB, GRUEN)

    define_gpios(ROT, GELB, GRUEN, False)

    # 5 Minuten Schleife
    while time.time() < t_end:
        try:
            ampel()
        except KeyboardInterrupt:
            print("Ampel ist manuell ausgeschaltet!")
            turn_off_all(ROT, GELB, GRUEN, 1)
            clean(ROT, GELB, GRUEN)
            sys.exit(0)
        
    turn_off_all(ROT, GELB, GRUEN, 1)
    clean(ROT, GELB, GRUEN)
    sys.exit(0)
    