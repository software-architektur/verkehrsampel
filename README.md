# Verkehrsampel

The aim of this project is to update a simulated real world traffic light using Continuous Integration / Continuous Delivery and Deployment (CI/CD).

## Required Hardware Components

- 3x Raspberry-Pi’s
- 6x LED + Jumper Cables
- 6x Resistance
- 2x Bread Boards

## Installation and Running

### Hardware

Connect the LEDs to the GPIO pins with the help of jumpers and bread boards according to this table:

| LED Colour| GPIO Pin  |
|-----------|-----------|
| Red       | 15        |
| Yellow    | 16        |
| Green     | 18        |


### Software

1. Install Docker on Raspberry-Pi's.
2. Create a Kubernetes cluster **(k3s)** with one master and two worker nodes.
3. Install Helm on master node.

### Runner Registration

Register the GitLab runner with the help of Helm.

**Note:** Assign the runner node selector to the master node.

### Development Environment

As usual, the development shall be done in a seperate branch e.g: `develop` branch, in which all code changes are made.


## CI/CD Process

After opening a pull request (i.e. merge request in GitLab) the unit-test pipeline is going to be automatically triggered. The changes are going to be automatically merged to the base branch after a successful unit-test.

After merging the changes to the `main` branch, a tag shall be created in order to automatically trigger the second pipeline. This pipeline has four stages:

```
1. Publish-and-release
2. Deployment-preparation
3. Deploy
4. Clean
```

The `Publish-and-release` stage handles the process of building the docker image and creating a new release of the software.

The `Deployment-preparation` stage dynamically registers a temporary runner and assign it to a specific target worker node to prepare it for deployment.

The `Deploy` stage deploys the new release with new software changes on the target traffic light and run it.

The `Clean` stage deletes the temporary runner.


## Deleting Tagged Release

Deleting created releases can only be done through GitLab API. Example: `curl --request DELETE --header "PRIVATE-TOKEN: <YOUR-TOKEN>" "https://gitlab.com/api/v4/projects/<PROJECT-ID>/releases/<TAG-ID>"`


## Extra Links

- [RPI GPIO installation](https://sourceforge.net/p/raspberry-gpio-python/wiki/install/)
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/pipelines/)