#!/bin/bash

for software in $(ls Software); do
    for target in $(ls Software/$software/Target); do
        echo "----------------------------------------------Delete Runner of: $target----------------------------------------------"
        helm delete --namespace $NAMESPACE deploy-on-$target
    done
done
