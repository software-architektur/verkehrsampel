#!/bin/bash
IMAGE="";
touch $1
{
echo "
stages:
  - deploy-on-node
"
for software in $(ls Software)
   do
   IMAGE="015774109121/softwarearchitectureproject:$software"
   for target in $(ls Software/$software/Target)
   do
   echo "run_job_on_$target:
  image: arm32v6/docker
  tags:
    - deploy-on-$target
  needs:
    - pipeline: \$PARENT_PIPELINE_ID
      job: register_Temp_Runner
  stage: deploy-on-node
  script:
    - docker run -d --rm --device /dev/gpiomem $IMAGE
    # - docker info
  allow_failure: true
   "
   done
done
}>$1

