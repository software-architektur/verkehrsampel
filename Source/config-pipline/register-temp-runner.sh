#!/bin/bash

for software in $(ls Software); do
    echo "+++++++++++++++++++++++++++++++++++++++++New Software: $software +++++++++++++++++++++++++++++++++++++++++"
    for target in $(ls Software/$software/Target); do
        echo "----------------------------------------------Register Runner for:  $target----------------------------------------------"
        helm install --namespace $NAMESPACE -f $CONFIG_FILE deploy-on-$target gitlab/gitlab-runner --set runners.tags="deploy-on-$target" --set    runners.nodeSelector."kubernetes\.io/hostname"=$target --set runnerRegistrationToken=$REGISTRATION_TOKEN
    done
done
