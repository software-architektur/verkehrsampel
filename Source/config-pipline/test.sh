#!/bin/bash
IMAGE="";
touch $1
{
echo "
stages:
  - test-software
"
for software in $(ls Software)
do
    echo "test-$software:
  image: resin/raspberry-pi-debian:buster
  tags:
    - $2
  needs:
    - pipeline: \$PARENT_PIPELINE_ID
      job: Prepare-Test
  stage: test-software
  before_script:
    - apt-get update
    - apt-get install python3 idle3
    - apt-get install --no-install-recommends python3-rpi.gpio
  script:
    - python3 Software/$software/test.py
   "
done
}>$1